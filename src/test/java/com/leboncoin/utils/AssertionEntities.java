package com.leboncoin.utils;

import org.junit.jupiter.api.Assertions;

import com.leboncoin.entities.Dev;

/**
 * The Class AssertionEntities.
 */
public final class AssertionEntities {

	/**
	 * Assert dev.
	 *
	 * @param expectedDev the expected dev
	 * @param actualDev   the actual dev
	 */
	public static void assertDev(Dev expectedDev, Dev actualDev) {
		Assertions.assertNotNull(actualDev);
		Assertions.assertNotNull(actualDev.getId());
		Assertions.assertNotNull(actualDev.getClass());
		Assertions.assertNotNull(actualDev.getTeam());
		Assertions.assertNotNull(actualDev.getDateElection());
		Assertions.assertEquals(expectedDev.getId(), actualDev.getId());
		Assertions.assertEquals(expectedDev.getPrenom(), actualDev.getPrenom());
		Assertions.assertEquals(expectedDev.getTeam(), actualDev.getTeam());
		Assertions.assertEquals(expectedDev.getDateElection(), actualDev.getDateElection());
	}
}
