package com.leboncoin.test.unitaires;

import java.text.ParseException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import com.leboncoin.entities.Dev;
import com.leboncoin.repos.DevRepository;
import com.leboncoin.utils.AssertionEntities;
import com.leboncoin.utils.DevUtils;

/**
 * The Class TestGetNextDevByPrenom.
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ActiveProfiles("test")
public class TestGetNextDevByPrenom {

	/** The dev repository. */
	@Autowired
	DevRepository devRepository;

	/** The Constant PRENOM_DEV. */
	private static final String PRENOM_DEV = "Renaud";

	/** The Constant TEAM_DEV. */
	private static final String TEAM_DEV = "Core Qualité";

	/** The Constant ID_DEV. */
	private static final int ID_DEV = 4;

	/** The Constant DATE_DEV. */
	private static final String DATE_DEV = "2021-04-24";

	/** The Constant NAME_RESEARCH. */
	private static final String NAME_RESEARCH = "Hugo";

	/** The Constant NO_NAME_AFTER. */
	private static final String NO_NAME_AFTER = "Saad";

	/** The dev expected. */
	private static Dev devExpected;

	/**
	 * Sets the dev.
	 *
	 * @throws ParseException the parse exception
	 */
	@BeforeAll
	public static void setDev() throws ParseException {
		devExpected = new Dev(ID_DEV, PRENOM_DEV, TEAM_DEV, DevUtils.parseDate(DATE_DEV));
	}

	/**
	 * Test get next dev by prenom.
	 */
	@Test
	public void testGetNextDevByPrenom() {
		Dev returnDev = devRepository.findFirstByPrenomGreaterThanOrderByPrenomAsc(NAME_RESEARCH);
		AssertionEntities.assertDev(devExpected, returnDev);
	}

	/**
	 * Test get next dev by prenom no date under date research.
	 */
	@Test
	public void testGetNextDevByPrenomNoDateUnderDateResearch() {
		Assertions.assertNull(devRepository.findFirstByPrenomGreaterThanOrderByPrenomAsc(NO_NAME_AFTER));
	}
}
