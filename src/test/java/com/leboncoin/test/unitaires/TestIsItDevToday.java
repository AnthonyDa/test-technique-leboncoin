package com.leboncoin.test.unitaires;

import java.text.ParseException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import com.leboncoin.entities.Dev;
import com.leboncoin.repos.DevRepository;
import com.leboncoin.utils.AssertionEntities;
import com.leboncoin.utils.DevUtils;

/**
 * The Class TestIsItDevToday.
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ActiveProfiles("test")
public class TestIsItDevToday {

	/** The dev repository. */
	@Autowired
	DevRepository devRepository;

	/** The Constant THERE_IS_DEV. */
	private static final String THERE_IS_DEV = "2021-04-26";

	/** The Constant THERE_IS_NO_DEV. */
	private static final String THERE_IS_NO_DEV = "2021-04-31";

	/** The dev expected. */
	private static Dev devExpected;

	/** The Constant PRENOM_DEV. */
	private static final String PRENOM_DEV = "Anthony";

	/** The Constant TEAM_DEV. */
	private static final String TEAM_DEV = "Core Qualité";

	/** The Constant ID_DEV. */
	private static final int ID_DEV = 6;

	/**
	 * Sets the dev.
	 *
	 * @throws ParseException the parse exception
	 */
	@BeforeAll
	public static void setDev() throws ParseException {
		devExpected = new Dev(ID_DEV, PRENOM_DEV, TEAM_DEV, DevUtils.parseDate(THERE_IS_DEV));
	}

	/**
	 * Test is it dev today.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testIsItDevToday() throws ParseException {
		Dev returnDev = devRepository.findByDateElection(DevUtils.parseDate(THERE_IS_DEV));
		AssertionEntities.assertDev(devExpected, returnDev);
	}

	/**
	 * Test is it dev today alternative.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testIsItDevTodayAlternative() throws ParseException {
		Assertions.assertNull(devRepository.findByDateElection(DevUtils.parseDate(THERE_IS_NO_DEV)));
	}
}
