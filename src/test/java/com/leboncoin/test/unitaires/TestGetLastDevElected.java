package com.leboncoin.test.unitaires;

import java.text.ParseException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import com.leboncoin.entities.Dev;
import com.leboncoin.repos.DevRepository;
import com.leboncoin.utils.AssertionEntities;
import com.leboncoin.utils.DevUtils;

/**
 * The Class TestGetLastDevElected.
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ActiveProfiles("test")
public class TestGetLastDevElected {

	/** The dev repository. */
	@Autowired
	DevRepository devRepository;

	/** The Constant PRENOM_DEV. */
	private static final String PRENOM_DEV = "Saad";

	/** The Constant TEAM_DEV. */
	private static final String TEAM_DEV = "Android";

	/** The Constant ID_DEV. */
	private static final int ID_DEV = 3;

	/** The Constant DATE_DEV. */
	private static final String DATE_DEV = "2021-04-23";

	/** The Constant DATE_OF_THE_DAY. */
	private static final String DATE_OF_THE_DAY = "2021-04-24";

	/** The Constant DATE_NO_DATE_UNDER. */
	private static final String DATE_NO_DATE_UNDER = "2021-04-21";

	/** The dev expected. */
	private static Dev devExpected;

	/**
	 * Sets the dev.
	 *
	 * @throws ParseException the parse exception
	 */
	@BeforeAll
	public static void setDev() throws ParseException {
		devExpected = new Dev(ID_DEV, PRENOM_DEV, TEAM_DEV, DevUtils.parseDate(DATE_DEV));
	}

	/**
	 * Test get date last dev elected.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testGetDateLastDevElected() throws ParseException {
		Dev returnDev = devRepository
				.findFirstByDateElectionLessThanOrderByDateElectionDesc(DevUtils.parseDate(DATE_OF_THE_DAY));
		AssertionEntities.assertDev(devExpected, returnDev);
	}

	/**
	 * Test no date under date research.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testNoDateUnderDateResearch() throws ParseException {
		Assertions.assertNull(devRepository
				.findFirstByDateElectionLessThanOrderByDateElectionDesc(DevUtils.parseDate(DATE_NO_DATE_UNDER)));
	}
}
