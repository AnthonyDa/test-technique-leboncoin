package com.leboncoin.test.controller;

import java.text.ParseException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.leboncoin.services.DevServicesImpl;

/**
 * The Class TestGetDevOfTheDay.
 */
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ActiveProfiles("test")
public class TestGetDevOfTheDay {

	/** The dev services impl. */
	@Autowired
	DevServicesImpl devServicesImpl;

	/** The Constant PRENOM_DEV. */
	private static final String PRENOM_DEV = "Fanch";

	/**
	 * Test get dev of the day.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testGetDevOfTheDay() throws ParseException {
		String prenomReturnDev = devServicesImpl.getDevOfTheDay();
		Assertions.assertEquals(PRENOM_DEV, prenomReturnDev);
	}
}
