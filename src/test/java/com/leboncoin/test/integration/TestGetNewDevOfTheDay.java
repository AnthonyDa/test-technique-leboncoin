package com.leboncoin.test.integration;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.leboncoin.entities.Dev;
import com.leboncoin.services.DevServicesImpl;
import com.leboncoin.utils.AssertionEntities;
import com.leboncoin.utils.DevUtils;

/**
 * The Class TestGetNewDevOfTheDay.
 */
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ActiveProfiles("test")
public class TestGetNewDevOfTheDay {

	/** The dev services impl. */
	@Autowired
	DevServicesImpl devServicesImpl;

	/** The Constant PRENOM_NEW_DEV. */
	private static final String PRENOM_NEW_DEV = "Anthony";

	/** The Constant TEAM_NEW_DEV. */
	private static final String TEAM_NEW_DEV = "Core Qualité";

	/** The Constant ID_NEW_DEV. */
	private static final int ID_NEW_DEV = 6;

	/** The Constant DATE_NEW_DEV. */
	private static final String DATE_NEW_DEV = "2021-04-26";

	/** The new dev expected. */
	private static Dev newDevExpected;

	/** The Constant PRENOM_LAST_DEV. */
	private static final String PRENOM_LAST_DEV = "Saad";

	/** The Constant TEAM_LAST_DEV. */
	private static final String TEAM_LAST_DEV = "Android";

	/** The Constant ID_LAST_DEV. */
	private static final int ID_LAST_DEV = 6;

	/** The Constant DATE_LAST_DEV. */
	private static final String DATE_LAST_DEV = "2021-04-23";

	/** The last dexpected. */
	private static Dev lastDexpected;

	/**
	 * Sets the dev.
	 *
	 * @throws ParseException the parse exception
	 */
	@BeforeAll
	public static void setDev() throws ParseException {
		newDevExpected = new Dev(ID_NEW_DEV, PRENOM_NEW_DEV, TEAM_NEW_DEV, DevUtils.parseDate(DATE_NEW_DEV));
		lastDexpected = new Dev(ID_LAST_DEV, PRENOM_LAST_DEV, TEAM_LAST_DEV, DevUtils.parseDate(DATE_LAST_DEV));
	}

	/**
	 * Test get new dev of the day.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testGetNewDevOfTheDay() throws ParseException {
		Dev returnDev = devServicesImpl.getNewDevOfTheDay(lastDexpected);
		AssertionEntities.assertDev(newDevExpected, returnDev);
	}

	/**
	 * Test get new dev of the day alternative.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testGetNewDevOfTheDayAlternative() throws ParseException {
		lastDexpected.setTeam(null);
		Dev returnDev = devServicesImpl.getNewDevOfTheDay(lastDexpected);
		AssertionEntities.assertDev(newDevExpected, returnDev);
	}

	/**
	 * Test get last dev echec.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testGetLastDevEchec() throws ParseException {
		Assertions.assertNotNull((NullPointerException) assertThrows(RuntimeException.class, () -> {
			devServicesImpl.getNewDevOfTheDay(null);
		}));
	}
}
