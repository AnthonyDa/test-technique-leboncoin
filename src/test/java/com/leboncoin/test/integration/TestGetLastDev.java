package com.leboncoin.test.integration;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;

import com.leboncoin.entities.Dev;
import com.leboncoin.services.DevServicesImpl;
import com.leboncoin.utils.AssertionEntities;
import com.leboncoin.utils.DevUtils;

/**
 * The Class TestGetLastDev.
 */
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ActiveProfiles("test")
public class TestGetLastDev {

	/** The dev services impl. */
	@Autowired
	DevServicesImpl devServicesImpl;

	/** The Constant PRENOM_DEV. */
	private static final String PRENOM_DEV = "Anthony";

	/** The Constant TEAM_DEV. */
	private static final String TEAM_DEV = "Core Qualité";

	/** The Constant ID_DEV. */
	private static final int ID_DEV = 6;

	/** The Constant DATE_DEV. */
	private static final String DATE_DEV = "2021-04-26";

	/** The dev expected. */
	private static Dev devExpected;

	/**
	 * Sets the dev.
	 *
	 * @throws ParseException the parse exception
	 */
	@BeforeAll
	public static void setDev() throws ParseException {
		devExpected = new Dev(ID_DEV, PRENOM_DEV, TEAM_DEV, DevUtils.parseDate(DATE_DEV));
	}

	/**
	 * Test get last dev.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testGetLastDev() throws ParseException {
		Dev returnDev = devServicesImpl.getLastDev(DevUtils.getNewDateWithPattern());
		AssertionEntities.assertDev(devExpected, returnDev);
	}

	/**
	 * Test get last dev echec.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testGetLastDevEchec() throws ParseException {
		Assertions.assertNotNull((InvalidDataAccessApiUsageException) assertThrows(RuntimeException.class, () -> {
			devServicesImpl.getLastDev(null);
		}));
	}
}
