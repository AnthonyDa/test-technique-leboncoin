package com.leboncoin.test.integration;

import java.text.ParseException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.leboncoin.entities.Dev;
import com.leboncoin.services.DevServicesImpl;
import com.leboncoin.utils.AssertionEntities;
import com.leboncoin.utils.DevUtils;

/**
 * The Class TestGetNewDevOfTheDayAndSetNewDateElection.
 */
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ActiveProfiles("test")
public class TestGetNewDevOfTheDayAndSetNewDateElection {

	/** The dev services impl. */
	@Autowired
	DevServicesImpl devServicesImpl;

	/** The Constant PRENOM_DEV. */
	private static final String PRENOM_DEV = "Fanch";

	/** The Constant TEAM_DEV. */
	private static final String TEAM_DEV = "Core Qualité";

	/** The Constant ID_DEV. */
	private static final int ID_DEV = 1;

	/** The dev expected. */
	private static Dev devExpected;

	/**
	 * Sets the dev.
	 *
	 * @throws ParseException the parse exception
	 */
	@BeforeAll
	public static void setDev() throws ParseException {
		devExpected = new Dev(ID_DEV, PRENOM_DEV, TEAM_DEV, DevUtils.getNewDateWithPattern());
	}

	/**
	 * Test get new dev of the day and set new date election.
	 *
	 * @throws ParseException the parse exception
	 */
	@Test
	public void testGetNewDevOfTheDayAndSetNewDateElection() throws ParseException {
		Dev returnDev = devServicesImpl.getNewDevOfTheDayAndSetNewDateElection(DevUtils.getNewDateWithPattern());
		AssertionEntities.assertDev(devExpected, returnDev);
	}
}
