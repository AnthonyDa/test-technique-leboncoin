package com.leboncoin.repos;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;

import com.leboncoin.entities.Dev;

/**
 * The Interface DevRepository.
 */
public interface DevRepository extends CrudRepository<Dev, Integer> {

	/**
	 * Find first dev by date election less than param order by date election desc.
	 *
	 * @param researchDate the research date
	 * @return the dev
	 */
	Dev findFirstByDateElectionLessThanOrderByDateElectionDesc(Date researchDate);

	/**
	 * Find first dev by prenom greater than param order by prenom asc.
	 *
	 * @param prenomDev the prenom dev
	 * @return the dev
	 */
	Dev findFirstByPrenomGreaterThanOrderByPrenomAsc(String prenomDev);

	/**
	 * Find first dev by order by prenom asc.
	 *
	 * @return the dev
	 */
	Dev findFirstByOrderByPrenomAsc();

	/**
	 * Find dev by date election param.
	 *
	 * @param date the date
	 * @return the dev
	 */
	Dev findByDateElection(Date date);
}
