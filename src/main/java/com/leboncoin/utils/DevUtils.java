package com.leboncoin.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The Class DevUtils.
 */
public class DevUtils {

	/** The Constant dateFormat. */
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	/** The new date. */
	private static Date newDate = new Date();

	/**
	 * Gets the new date with pattern "yyyy-MM-dd".
	 *
	 * @return the new date with pattern
	 * @throws ParseException the parse exception
	 */
	public static Date getNewDateWithPattern() throws ParseException {
		String stringifiedDate = dateFormat.format(newDate);
		return dateFormat.parse(stringifiedDate);
	}

	/**
	 * Parses the date with pram string.
	 *
	 * @param dateString the date string
	 * @return the date
	 * @throws ParseException the parse exception
	 */
	public static Date parseDate(String dateString) throws ParseException {
		return dateFormat.parse(dateString);
	}
}
