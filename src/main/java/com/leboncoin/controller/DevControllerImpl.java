package com.leboncoin.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.leboncoin.services.DevServicesImpl;

/**
 * The Class DevController.
 */
@RestController
public class DevControllerImpl implements DevControllerApi {
	
	/** The dev services impl. */
	@Autowired
	DevServicesImpl devServicesImpl;

	/**
	 * Gets the dev of the day.
	 *
	 * @return the dev of the day
	 * @throws ParseException the parse exception
	 */
	@PostMapping(path="/firefighter/new", produces ="application/json")
	public String getPrenomDevOfTheDay() throws ParseException {
		return devServicesImpl.getDevOfTheDay();
	}
}
