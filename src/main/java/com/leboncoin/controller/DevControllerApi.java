package com.leboncoin.controller;

import java.text.ParseException;

/**
 * The Interface DevControllerApi.
 */
public interface DevControllerApi {

	/**
	 * Gets the prenom dev of the day.
	 *
	 * @return the prenom dev of the day
	 * @throws ParseException
	 */
	String getPrenomDevOfTheDay() throws ParseException;
}
