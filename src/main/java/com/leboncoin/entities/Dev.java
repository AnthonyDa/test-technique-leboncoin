package com.leboncoin.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class Dev.
 */
@Entity
@Table(name = "dev")
public class Dev {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	/** The prenom. */
	private String prenom;

	/** The team. */
	private String team;

	/** The date election. */
	@Temporal(TemporalType.DATE)
	private Date dateElection;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the prenom.
	 *
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Sets the prenom.
	 *
	 * @param prenom the new prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Gets the team.
	 *
	 * @return the team
	 */
	public String getTeam() {
		return team;
	}

	/**
	 * Sets the team.
	 *
	 * @param team the new team
	 */
	public void setTeam(String team) {
		this.team = team;
	}

	/**
	 * Gets the date election.
	 *
	 * @return the date election
	 */
	public Date getDateElection() {
		return dateElection;
	}

	/**
	 * Sets the date election.
	 *
	 * @param dateElection the new date election
	 */
	public void setDateElection(Date dateElection) {
		this.dateElection = dateElection;
	}

	/**
	 * Instantiates a new dev.
	 */
	public Dev() {
		super();
	}

	/**
	 * Instantiates a new dev.
	 *
	 * @param id           the id
	 * @param prenom       the prenom
	 * @param team         the team
	 * @param dateElection the date election
	 */
	public Dev(int id, String prenom, String team, Date dateElection) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.team = team;
		this.dateElection = dateElection;
	}
}
