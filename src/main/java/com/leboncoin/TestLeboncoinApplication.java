package com.leboncoin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class TestLeboncoinApplication.
 */
@SpringBootApplication
public class TestLeboncoinApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(TestLeboncoinApplication.class, args);
	}
}
