package com.leboncoin.services;

import java.text.ParseException;

/**
 * The Interface DevServicesApi.
 */
public interface DevServicesApi {

	/**
	 * Gets the dev of the day.
	 *
	 * @return the prenom dev of the day
	 * @throws ParseException the parse exception
	 */
	String getDevOfTheDay() throws ParseException;
}
