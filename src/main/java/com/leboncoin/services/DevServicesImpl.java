package com.leboncoin.services;

import java.text.ParseException;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leboncoin.entities.Dev;
import com.leboncoin.repos.DevRepository;
import com.leboncoin.utils.DevUtils;

/**
 * The Class DevServicesImpl.
 */
@Service
@Transactional
public class DevServicesImpl implements DevServicesApi {

	/** The dev repository. */
	@Autowired
	DevRepository devRepository;

	/**
	 * Gets the dev of the day.
	 *
	 * @return the prenom dev of the day
	 * @throws ParseException the parse exception
	 */
	@Override
	public String getDevOfTheDay() throws ParseException {
		Date dateOfTheDay = DevUtils.getNewDateWithPattern();
		Dev devOfTheDay = devRepository.findByDateElection(dateOfTheDay);
		if (devOfTheDay == null) {
			return getNewDevOfTheDayAndSetNewDateElection(dateOfTheDay).getPrenom();
		} else {
			return devOfTheDay.getPrenom();
		}
	}

	/**
	 * Gets the last dev.
	 *
	 * @param dateOfTheDay the date of the day
	 * @return the last dev
	 */
	public Dev getLastDev(Date dateOfTheDay) {
		Dev lastDev = devRepository.findFirstByDateElectionLessThanOrderByDateElectionDesc(dateOfTheDay);
		if (lastDev == null) {
			return findFirstAlphabeticalDev();
		} else {
			return lastDev;
		}
	}

	/**
	 * Gets the new dev of the day.
	 *
	 * @param lastDev the last dev
	 * @return the new dev of the day
	 */
	public Dev getNewDevOfTheDay(Dev lastDev) {
		Dev newDevOfTheDay = devRepository.findFirstByPrenomGreaterThanOrderByPrenomAsc(lastDev.getPrenom());
		if (newDevOfTheDay == null) {
			return findFirstAlphabeticalDev();
		} else {
			return newDevOfTheDay;
		}
	}

	/**
	 * Find first alphabetical dev by prenom.
	 *
	 * @return the dev
	 */
	public Dev findFirstAlphabeticalDev() {
		return devRepository.findFirstByOrderByPrenomAsc();
	}

	/**
	 * Gets the new dev of the day and sets new date election.
	 *
	 * @param dateOfTheDay the date of the day
	 * @return the new dev of the daywith his new date election
	 */
	public Dev getNewDevOfTheDayAndSetNewDateElection(Date dateOfTheDay) {
		Dev lastDev = getLastDev(dateOfTheDay);
		Dev newDevOfTheDay = getNewDevOfTheDay(lastDev);
		newDevOfTheDay.setDateElection(dateOfTheDay);
		return devRepository.save(newDevOfTheDay);
	}
}
